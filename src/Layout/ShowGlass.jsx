import React, { Component } from "react";
import { connect } from "react-redux";

class ShowGlass extends Component {
  render() {
    return (
      <div className="detail">
        <img src={this.props.detail.url} alt="" className="glass-img" />
        <div className="detail__content bg-dark">
          <p className="detail__name text-white">{this.props.detail.name}</p>
          <p className="text-white">{this.props.detail.desc}</p>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.glassReducer.content,
  };
};

export default connect(mapStateToProps)(ShowGlass);
