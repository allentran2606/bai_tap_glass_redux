import React, { Component } from "react";

export default class Model extends Component {
  render() {
    return (
      <div>
        <div className="title py-4 mx-auto bg-dark text-white display-4">
          The Glasses Online
        </div>
        <div className="row justify-content-center align-items-center">
          <img
            className="col-5 mr-5"
            style={{ width: "200px", height: "500px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
          <img
            className="col-5 ml-5  position-relative"
            style={{ width: "200px", height: "500px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
        </div>
      </div>
    );
  }
}
