import React, { Component } from "react";
import GlassList from "./GlassList";
import Model from "./Model";
import ShowGlass from "./ShowGlass";

export default class Layout extends Component {
  render() {
    return (
      <div className="container">
        <Model />
        <GlassList />
        <ShowGlass />;
      </div>
    );
  }
}
