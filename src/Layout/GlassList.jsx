import React, { Component } from "react";
import { connect } from "react-redux";
import GlassItem from "./GlassItem";

class ListGlass extends Component {
  renderListGlass = () => {
    return this.props.glassArr.map((item) => {
      return <GlassItem data={item} />;
    });
  };

  render() {
    return <div className="row">{this.renderListGlass()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    glassArr: state.glassReducer.dataGlass,
    detail: state.glassReducer.content,
  };
};

export default connect(mapStateToProps)(ListGlass);
