import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_DETAIL } from "../redux/constant/constant";

class GlassItem extends Component {
  render() {
    let { url } = this.props.data;
    return (
      <div className="col-3 py-3">
        <div>
          <img
            style={{ width: "100%" }}
            src={url}
            alt=""
            onClick={() => {
              this.props.changeDetail(this.props.data);
            }}
          />
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    changeDetail: (detail) => {
      dispatch({
        type: CHANGE_DETAIL,
        payload: detail,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(GlassItem);
