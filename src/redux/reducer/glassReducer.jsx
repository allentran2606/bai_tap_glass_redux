import dataGlass from "../../dataGlasses.json";
import { CHANGE_DETAIL } from "../constant/constant";

let initialState = {
  dataGlass,
  content: dataGlass[0],
};

export const glassReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_DETAIL: {
      return { ...state, content: payload };
    }
    default:
      return state;
  }
};
