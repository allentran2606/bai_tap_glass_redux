import { glassReducer } from "./glassReducer";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({
  glassReducer: glassReducer,
});
