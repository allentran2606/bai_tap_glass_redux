import "./App.css";
import Layout from "./Layout/Layout";
// import RenderGlass from "./Layout/RenderGlass";

function App() {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
